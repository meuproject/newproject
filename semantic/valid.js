/**
 * Created by remersa on 05/08/16.
 */

var validationObj = {
    PrimeiroNome: {
        identifier : 'first-name',
        rules: [
            {
                type    : 'empty',
                prompt  : 'Please enter your first name'
            }
        ]
    },
    UltimoNome: {
        identifier : 'last-name',
        rules [
            {
                type    : 'empty',
                prompt  : 'Please enter your last name'
            }

        ]
    },

    username: {
        identifier: 'username',
        rules [
            {
                type: 'empty',
                prompt: 'Please enter a username'
            }

        ]
    },

    password: {
        identifier: 'password',
        rules [
            {
                type: 'empty',
                prompt: 'Please enter a password'
            },
            {
                type    : 'lenght[6]',
                prompt  : 'Your password must be at least 6 character'
            }

        ]

    }

}
